EESchema Schematic File Version 4
LIBS:A60 tetrode-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L A60~tetrode:tetrode_unit U4
U 1 1 5D6B2C69
P -1450 -350
F 0 "U4" H -1656 125 50  0000 C CNN
F 1 "tetrode_unit" H -1656 34  50  0000 C CNN
F 2 "A60 tetrode:tetrode" H -1150 -150 50  0001 C CNN
F 3 "" H -1150 -150 50  0001 C CNN
	1    -1450 -350
	0    1    1    0   
$EndComp
$Comp
L A60~tetrode:tetrode_unit U10
U 1 1 5D6B2FF3
P -1400 1900
F 0 "U10" H -1606 2375 50  0000 C CNN
F 1 "tetrode_unit" H -1606 2284 50  0000 C CNN
F 2 "A60 tetrode:tetrode" H -1100 2100 50  0001 C CNN
F 3 "" H -1100 2100 50  0001 C CNN
	1    -1400 1900
	0    1    1    0   
$EndComp
$Comp
L A60~tetrode:tetrode_unit U1
U 1 1 5D6B3145
P -2500 -1550
F 0 "U1" H -2706 -1075 50  0000 C CNN
F 1 "tetrode_unit" H -2706 -1166 50  0000 C CNN
F 2 "A60 tetrode:tetrode" H -2200 -1350 50  0001 C CNN
F 3 "" H -2200 -1350 50  0001 C CNN
	1    -2500 -1550
	0    1    1    0   
$EndComp
$Comp
L A60~tetrode:tetrode_unit U7
U 1 1 5D6B326C
P -2250 850
F 0 "U7" H -2456 1325 50  0000 C CNN
F 1 "tetrode_unit" H -2456 1234 50  0000 C CNN
F 2 "A60 tetrode:tetrode" H -1950 1050 50  0001 C CNN
F 3 "" H -1950 1050 50  0001 C CNN
	1    -2250 850 
	0    1    1    0   
$EndComp
$Comp
L A60~tetrode:tetrode_unit U12
U 1 1 5D6B338A
P -2250 3200
F 0 "U12" H -2456 3675 50  0000 C CNN
F 1 "tetrode_unit" H -2456 3584 50  0000 C CNN
F 2 "A60 tetrode:tetrode" H -1950 3400 50  0001 C CNN
F 3 "" H -1950 3400 50  0001 C CNN
	1    -2250 3200
	0    1    1    0   
$EndComp
$Comp
L A60~tetrode:tetrode_unit U3
U 1 1 5D6B3518
P -3650 -350
F 0 "U3" H -3856 125 50  0000 C CNN
F 1 "tetrode_unit" H -3856 34  50  0000 C CNN
F 2 "A60 tetrode:tetrode" H -3350 -150 50  0001 C CNN
F 3 "" H -3350 -150 50  0001 C CNN
	1    -3650 -350
	0    1    1    0   
$EndComp
$Comp
L A60~tetrode:tetrode_unit U8
U 1 1 5D6B3942
P -3650 1850
F 0 "U8" H -3856 2325 50  0000 C CNN
F 1 "tetrode_unit" H -3856 2234 50  0000 C CNN
F 2 "A60 tetrode:tetrode" H -3350 2050 50  0001 C CNN
F 3 "" H -3350 2050 50  0001 C CNN
	1    -3650 1850
	0    1    1    0   
$EndComp
$Comp
L A60~tetrode:tetrode_unit U2
U 1 1 5D6B3BCD
P -4800 -1500
F 0 "U2" H -5006 -1025 50  0000 C CNN
F 1 "tetrode_unit" H -5006 -1116 50  0000 C CNN
F 2 "A60 tetrode:tetrode" H -4500 -1300 50  0001 C CNN
F 3 "" H -4500 -1300 50  0001 C CNN
	1    -4800 -1500
	0    1    1    0   
$EndComp
$Comp
L A60~tetrode:tetrode_unit U6
U 1 1 5D6B3E32
P -4850 850
F 0 "U6" H -5056 1325 50  0000 C CNN
F 1 "tetrode_unit" H -5056 1234 50  0000 C CNN
F 2 "A60 tetrode:tetrode" H -4550 1050 50  0001 C CNN
F 3 "" H -4550 1050 50  0001 C CNN
	1    -4850 850 
	0    1    1    0   
$EndComp
$Comp
L A60~tetrode:tetrode_unit U11
U 1 1 5D6B40FD
P -4850 3200
F 0 "U11" H -5056 3675 50  0000 C CNN
F 1 "tetrode_unit" H -5056 3584 50  0000 C CNN
F 2 "A60 tetrode:tetrode" H -4550 3400 50  0001 C CNN
F 3 "" H -4550 3400 50  0001 C CNN
	1    -4850 3200
	0    1    1    0   
$EndComp
$Comp
L A60~tetrode:tetrode_unit U5
U 1 1 5D6B434B
P -5800 -300
F 0 "U5" H -6006 175 50  0000 C CNN
F 1 "tetrode_unit" H -6006 84  50  0000 C CNN
F 2 "A60 tetrode:tetrode" H -5500 -100 50  0001 C CNN
F 3 "" H -5500 -100 50  0001 C CNN
	1    -5800 -300
	0    1    1    0   
$EndComp
$Comp
L A60~tetrode:tetrode_unit U9
U 1 1 5D6B4673
P -5900 1900
F 0 "U9" H -6106 2375 50  0000 C CNN
F 1 "tetrode_unit" H -6106 2284 50  0000 C CNN
F 2 "A60 tetrode:tetrode" H -5600 2100 50  0001 C CNN
F 3 "" H -5600 2100 50  0001 C CNN
	1    -5900 1900
	0    1    1    0   
$EndComp
Wire Wire Line
	-3550 1750 -3500 1750
Wire Wire Line
	-3750 1750 -3800 1750
Wire Wire Line
	-3550 2350 -3500 2350
Wire Wire Line
	-3500 2350 -3500 6000
Wire Wire Line
	-3550 1950 -3550 2350
Wire Wire Line
	-3500 1750 -3500 2200
Wire Wire Line
	-3500 2200 -3450 2200
Wire Wire Line
	-3450 2200 -3450 6400
Wire Wire Line
	-3400 1900 -3400 6000
Wire Wire Line
	-3750 1950 -3750 2100
Wire Wire Line
	-3750 2100 -3600 2100
Wire Wire Line
	-3600 3150 -3550 3150
Wire Wire Line
	-3600 2100 -3600 3150
Wire Wire Line
	-3800 1750 -3800 3200
Wire Wire Line
	-3800 3200 -3600 3200
Wire Wire Line
	-3600 3200 -3600 6000
Wire Wire Line
	-3350 850  -3350 6400
Wire Wire Line
	-3800 850  -3350 850 
Wire Wire Line
	-3750 -450 -3800 -450
Wire Wire Line
	-3800 -450 -3800 850 
Wire Wire Line
	-3750 -250 -3750 800 
Wire Wire Line
	-3750 800  -3300 800 
Wire Wire Line
	-3300 800  -3300 6000
Wire Wire Line
	-3500 750  -3250 750 
Wire Wire Line
	-3250 750  -3250 6400
Wire Wire Line
	-3200 6000 -3200 700 
Wire Wire Line
	-3200 700  -3450 700 
Wire Wire Line
	-3450 700  -3450 -450
Wire Wire Line
	-3450 -450 -3550 -450
Wire Wire Line
	-3500 -250 -3550 -250
Wire Wire Line
	-3500 -250 -3500 750 
Wire Wire Line
	-3400 650  -3150 650 
Wire Wire Line
	-3150 650  -3150 6400
Wire Wire Line
	-4600 3250 -4600 3600
Wire Wire Line
	-4600 3600 -3650 3600
Wire Wire Line
	-3700 6000 -3700 3750
Wire Wire Line
	-3700 3750 -4650 3750
Wire Wire Line
	-4650 3750 -4650 3100
Wire Wire Line
	-4650 3100 -4750 3100
Wire Wire Line
	-4750 3300 -4750 3850
Wire Wire Line
	-4750 3850 -3750 3850
Wire Wire Line
	-3800 6000 -3800 3950
Wire Wire Line
	-3800 3950 -4950 3950
Wire Wire Line
	-4950 3950 -4950 3300
Wire Wire Line
	-4950 3100 -5000 3100
Wire Wire Line
	-5000 3100 -5000 4150
Wire Wire Line
	-5000 4150 -3900 4150
Wire Wire Line
	-3900 4150 -3900 6000
Wire Wire Line
	-2350 3100 -2400 3100
Wire Wire Line
	-2400 5100 -3100 5100
Wire Wire Line
	-3100 5100 -3100 6000
Wire Wire Line
	-2400 3100 -2400 5100
Wire Wire Line
	-3050 6400 -3050 5150
Wire Wire Line
	-3050 5150 -2350 5150
Wire Wire Line
	-2350 5150 -2350 3300
Wire Wire Line
	-2150 3300 -2150 5300
Wire Wire Line
	-2150 5300 -3000 5300
Wire Wire Line
	-3000 5300 -3000 6000
Wire Wire Line
	-2100 7000 -2100 3100
Wire Wire Line
	-2100 3100 -2150 3100
Wire Wire Line
	-2100 7100 -2000 7100
Wire Wire Line
	-2000 7100 -2000 3250
Wire Wire Line
	-1500 1800 -1550 1800
Wire Wire Line
	-1550 7150 -2550 7150
Wire Wire Line
	-1550 1800 -1550 7150
Wire Wire Line
	-1500 2000 -1500 7200
Wire Wire Line
	-1500 7200 -2100 7200
Wire Wire Line
	-1300 2000 -1300 2200
Wire Wire Line
	-1300 2200 -1450 2200
Wire Wire Line
	-1450 2200 -1450 7250
Wire Wire Line
	-1450 7250 -2550 7250
Wire Wire Line
	-1300 1800 -1250 1800
Wire Wire Line
	-1250 1800 -1250 2800
Wire Wire Line
	-1250 2800 -1400 2800
Wire Wire Line
	-1400 2800 -1400 7300
Wire Wire Line
	-1400 7300 -2100 7300
Wire Wire Line
	-1150 1950 -1150 3000
Wire Wire Line
	-1150 3000 -1350 3000
Wire Wire Line
	-1350 3000 -1350 7350
Wire Wire Line
	-1350 7350 -2550 7350
Wire Wire Line
	-2350 750  -2400 750 
Wire Wire Line
	-2400 750  -2400 1350
Wire Wire Line
	-2400 1350 -900 1350
Wire Wire Line
	-900 7400 -2100 7400
Wire Wire Line
	-900 1350 -900 7400
Wire Wire Line
	-2350 950  -2350 1300
Wire Wire Line
	-2350 1300 -800 1300
Wire Wire Line
	-800 1300 -800 7450
Wire Wire Line
	-2150 950  -2150 1250
Wire Wire Line
	-2150 1250 -750 1250
Wire Wire Line
	-750 1250 -750 7500
Wire Wire Line
	-750 7500 -2100 7500
Wire Wire Line
	-2150 750  -2100 750 
Wire Wire Line
	-2100 750  -2100 1200
Wire Wire Line
	-2100 1200 -700 1200
Wire Wire Line
	-700 7600 -2100 7600
Wire Wire Line
	-700 1200 -700 7600
Wire Wire Line
	-2000 900  -650 900 
Wire Wire Line
	-650 900  -650 7700
Wire Wire Line
	-650 7700 -2100 7700
Wire Wire Line
	-5650 7000 -4900 7000
Wire Wire Line
	-5800 1800 -5700 1800
Wire Wire Line
	-5700 1800 -5700 7100
Wire Wire Line
	-5700 7100 -4900 7100
Wire Wire Line
	-5800 7150 -4450 7150
Wire Wire Line
	-5800 2000 -5800 7150
Wire Wire Line
	-5650 1950 -5650 7000
Wire Wire Line
	-6000 2000 -6000 7200
Wire Wire Line
	-6000 7200 -4900 7200
Wire Wire Line
	-6000 1800 -6050 1800
Wire Wire Line
	-6050 1800 -6050 7250
Wire Wire Line
	-6050 7250 -4450 7250
Wire Wire Line
	-4600 900  -4600 1200
Wire Wire Line
	-4600 1200 -6300 1200
Wire Wire Line
	-6300 1200 -6300 7300
Wire Wire Line
	-6300 7300 -4900 7300
Wire Wire Line
	-4750 750  -4700 750 
Wire Wire Line
	-4700 750  -4700 1150
Wire Wire Line
	-4700 1150 -6350 1150
Wire Wire Line
	-6350 1150 -6350 7350
Wire Wire Line
	-4750 950  -4750 1100
Wire Wire Line
	-4750 1100 -6400 1100
Wire Wire Line
	-6400 7400 -4900 7400
Wire Wire Line
	-6400 1100 -6400 7400
Wire Wire Line
	-4950 950  -6450 950 
Wire Wire Line
	-6450 950  -6450 7450
Wire Wire Line
	-4950 750  -5000 750 
Wire Wire Line
	-5000 750  -5000 900 
Wire Wire Line
	-5000 900  -6500 900 
Wire Wire Line
	-6500 900  -6500 7500
Wire Wire Line
	-6500 7500 -4900 7500
$Comp
L A60~tetrode:AZ8connector U13
U 1 1 5D65E4D2
P -3450 7350
F 0 "U13" H -1772 7346 50  0000 L CNN
F 1 "AZ8connector" H -1772 7255 50  0000 L CNN
F 2 "A60 tetrode:AZ8_connector" H -3750 7350 50  0001 C CNN
F 3 "" H -3750 7350 50  0001 C CNN
	1    -3450 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	-6350 7350 -4550 7350
Wire Wire Line
	-4550 7350 -4550 7450
Wire Wire Line
	-4550 7450 -4450 7450
Wire Wire Line
	-6450 7450 -4600 7450
Wire Wire Line
	-4600 7450 -4600 7550
Wire Wire Line
	-4600 7550 -4450 7550
Wire Wire Line
	-3450 6400 -3550 6400
Wire Wire Line
	-3550 3150 -3550 6300
Wire Wire Line
	-3550 6300 -3650 6300
Wire Wire Line
	-3650 6300 -3650 6400
Wire Wire Line
	-3750 6400 -3750 6250
Wire Wire Line
	-3750 6250 -3650 6250
Wire Wire Line
	-3650 3600 -3650 6250
Wire Wire Line
	-3750 3850 -3750 6200
Wire Wire Line
	-3750 6200 -3850 6200
Wire Wire Line
	-3850 6200 -3850 6400
Wire Wire Line
	-2550 7350 -2550 7450
Wire Wire Line
	-2550 7550 -2450 7550
Wire Wire Line
	-2450 7550 -2450 7450
Wire Wire Line
	-2450 7450 -800 7450
Wire Wire Line
	-5550 -250 -5550 800 
Wire Wire Line
	-5550 800  -6600 800 
Wire Wire Line
	-6600 800  -6600 7600
Wire Wire Line
	-6600 7600 -4900 7600
Wire Wire Line
	-5700 -400 -5600 -400
Wire Wire Line
	-5600 -400 -5600 700 
Wire Wire Line
	-5600 700  -6700 700 
Wire Wire Line
	-6700 7700 -4900 7700
Wire Wire Line
	-6700 700  -6700 7700
Wire Wire Line
	-5700 650  -6750 650 
Wire Wire Line
	-6750 8800 -3900 8800
Wire Wire Line
	-6750 650  -6750 8800
Wire Wire Line
	-6800 9100 -3850 9100
Wire Wire Line
	-3850 9100 -3850 8350
Wire Wire Line
	-6800 600  -6800 9100
Wire Wire Line
	-5900 -400 -5950 -400
Wire Wire Line
	-5950 -400 -5950 500 
Wire Wire Line
	-5950 500  -6850 500 
Wire Wire Line
	-6850 9150 -3800 9150
Wire Wire Line
	-3800 9150 -3800 8800
Wire Wire Line
	-6850 500  -6850 9150
Wire Wire Line
	-1550 -450 -1600 -450
Wire Wire Line
	-1600 -450 -1600 800 
Wire Wire Line
	-1600 800  -600 800 
Wire Wire Line
	-600 800  -600 8800
Wire Wire Line
	-600 8800 -3000 8800
Wire Wire Line
	-1550 -250 -1550 750 
Wire Wire Line
	-1550 750  -550 750 
Wire Wire Line
	-550 8950 -3050 8950
Wire Wire Line
	-3050 8950 -3050 8350
Wire Wire Line
	-550 750  -550 8950
Wire Wire Line
	-1350 -250 -1350 700 
Wire Wire Line
	-1350 700  -500 700 
Wire Wire Line
	-500 9100 -3100 9100
Wire Wire Line
	-3100 9100 -3100 8800
Wire Wire Line
	-500 700  -500 9100
Wire Wire Line
	-1350 -450 -1300 -450
Wire Wire Line
	-1300 -450 -1300 650 
Wire Wire Line
	-1300 650  -450 650 
Wire Wire Line
	-450 9150 -3150 9150
Wire Wire Line
	-3150 9150 -3150 8350
Wire Wire Line
	-450 650  -450 9150
Wire Wire Line
	-3200 8800 -3200 9250
Wire Wire Line
	-3200 9250 -400 9250
Wire Wire Line
	-400 9250 -400 -300
Wire Wire Line
	-400 -300 -1200 -300
Wire Wire Line
	-2600 -1650 -2650 -1650
Wire Wire Line
	-2650 -1650 -2650 -750
Wire Wire Line
	-2650 -750 -350 -750
Wire Wire Line
	-350 -750 -350 9400
Wire Wire Line
	-350 9400 -3250 9400
Wire Wire Line
	-3250 9400 -3250 8350
Wire Wire Line
	-2600 -1450 -2600 -800
Wire Wire Line
	-2600 -800 -300 -800
Wire Wire Line
	-300 9450 -3300 9450
Wire Wire Line
	-3300 9450 -3300 8800
Wire Wire Line
	-300 -800 -300 9450
Wire Wire Line
	-3350 8350 -3350 9500
Wire Wire Line
	-3350 9500 -250 9500
Wire Wire Line
	-250 9500 -250 -850
Wire Wire Line
	-250 -850 -2400 -850
Wire Wire Line
	-2400 -850 -2400 -1450
Wire Wire Line
	-2400 -1650 -2350 -1650
Wire Wire Line
	-2350 -1650 -2350 -900
Wire Wire Line
	-2350 -900 -200 -900
Wire Wire Line
	-200 9600 -3400 9600
Wire Wire Line
	-3400 9600 -3400 8800
Wire Wire Line
	-200 -900 -200 9600
Wire Wire Line
	-4900 -1600 -4950 -1600
Wire Wire Line
	-7500 -1050 -7500 9300
Wire Wire Line
	-7500 9300 -3750 9300
Wire Wire Line
	-3750 9300 -3750 8350
Wire Wire Line
	-3700 8800 -3700 9350
Wire Wire Line
	-3700 9350 -7600 9350
Wire Wire Line
	-7600 9350 -7600 -1250
Wire Wire Line
	-7600 -1250 -4650 -1250
Wire Wire Line
	-4650 -1250 -4650 -1600
Wire Wire Line
	-4650 -1600 -4700 -1600
Wire Wire Line
	-4550 -1050 -4550 -1450
Wire Wire Line
	-7500 -1050 -4550 -1050
Wire Wire Line
	-4700 -1400 -4700 -1300
Wire Wire Line
	-4700 -1300 -7650 -1300
Wire Wire Line
	-7650 -1300 -7650 9400
Wire Wire Line
	-7650 9400 -3650 9400
Wire Wire Line
	-3650 9400 -3650 8350
Wire Wire Line
	-3600 8800 -3600 9450
Wire Wire Line
	-7700 9450 -7700 -1400
Wire Wire Line
	-7700 -1400 -4900 -1400
Wire Wire Line
	-7700 9450 -3600 9450
Wire Wire Line
	-4950 -1600 -4950 -1450
Wire Wire Line
	-4950 -1450 -7750 -1450
Wire Wire Line
	-7750 9600 -3550 9600
Wire Wire Line
	-3550 9600 -3550 8350
Wire Wire Line
	-7750 -1450 -7750 9600
Wire Wire Line
	-3400 -300 -3400 650 
Wire Wire Line
	-2250 -950 -150 -950
Wire Wire Line
	-150 9700 -3500 9700
Wire Wire Line
	-3500 9700 -3500 8800
Wire Wire Line
	-150 -950 -150 9700
Wire Wire Line
	-2250 -1500 -2250 -950
Wire Wire Line
	-5750 600  -5750 -50 
Wire Wire Line
	-5750 -50  -5700 -50 
Wire Wire Line
	-5700 -50  -5700 -200
Wire Wire Line
	-6800 600  -5750 600 
Wire Wire Line
	-5700 650  -5700 400 
Wire Wire Line
	-5700 400  -5900 400 
Wire Wire Line
	-5900 400  -5900 -200
$EndSCHEMATC
