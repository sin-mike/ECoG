EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:samtec_molc_110_01_s_q
LIBS:electrode
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SAMTEC_MOLC_110_01_S_Q MOLC1
U 1 1 5A816A8E
P 5700 1850
F 0 "MOLC1" H 5700 2450 60  0000 C CNN
F 1 "SAMTEC_MOLC_110_01_S_Q" H 5750 2600 60  0000 C CNN
F 2 "SAMTEC:SAMTEC_MOLC_110_01_s_q" H 5750 2600 60  0001 C CNN
F 3 "" H 5750 2600 60  0001 C CNN
	1    5700 1850
	1    0    0    -1  
$EndComp
$Comp
L electrode U1
U 1 1 5A81C19F
P 5700 1850
F 0 "U1" H 5600 1850 60  0000 C CNN
F 1 "electrode" H 6500 1850 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 1850 60  0001 C CNN
F 3 "" H 5700 1850 60  0001 C CNN
	1    5700 1850
	-1   0    0    -1  
$EndComp
$Comp
L electrode U2
U 1 1 5A81C295
P 5700 1950
F 0 "U2" H 5600 1950 60  0000 C CNN
F 1 "electrode" H 6500 1950 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 1950 60  0001 C CNN
F 3 "" H 5700 1950 60  0001 C CNN
	1    5700 1950
	-1   0    0    -1  
$EndComp
$Comp
L electrode U3
U 1 1 5A81C2D6
P 5700 2050
F 0 "U3" H 5600 2050 60  0000 C CNN
F 1 "electrode" H 6500 2050 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 2050 60  0001 C CNN
F 3 "" H 5700 2050 60  0001 C CNN
	1    5700 2050
	-1   0    0    -1  
$EndComp
$Comp
L electrode U4
U 1 1 5A81C2F2
P 5700 2150
F 0 "U4" H 5600 2150 60  0000 C CNN
F 1 "electrode" H 6500 2150 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 2150 60  0001 C CNN
F 3 "" H 5700 2150 60  0001 C CNN
	1    5700 2150
	-1   0    0    -1  
$EndComp
$Comp
L electrode U5
U 1 1 5A81C311
P 5700 2250
F 0 "U5" H 5600 2250 60  0000 C CNN
F 1 "electrode" H 6500 2250 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 2250 60  0001 C CNN
F 3 "" H 5700 2250 60  0001 C CNN
	1    5700 2250
	-1   0    0    -1  
$EndComp
$Comp
L electrode U6
U 1 1 5A81C333
P 5700 2350
F 0 "U6" H 5600 2350 60  0000 C CNN
F 1 "electrode" H 6500 2350 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 2350 60  0001 C CNN
F 3 "" H 5700 2350 60  0001 C CNN
	1    5700 2350
	-1   0    0    -1  
$EndComp
$Comp
L electrode U7
U 1 1 5A81C454
P 5700 2450
F 0 "U7" H 5600 2450 60  0000 C CNN
F 1 "electrode" H 6500 2450 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 2450 60  0001 C CNN
F 3 "" H 5700 2450 60  0001 C CNN
	1    5700 2450
	-1   0    0    -1  
$EndComp
$Comp
L electrode U8
U 1 1 5A81C47C
P 5700 2550
F 0 "U8" H 5600 2550 60  0000 C CNN
F 1 "electrode" H 6500 2550 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 2550 60  0001 C CNN
F 3 "" H 5700 2550 60  0001 C CNN
	1    5700 2550
	-1   0    0    -1  
$EndComp
$Comp
L electrode U9
U 1 1 5A81C4A7
P 5700 2650
F 0 "U9" H 5600 2650 60  0000 C CNN
F 1 "electrode" H 6500 2650 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 2650 60  0001 C CNN
F 3 "" H 5700 2650 60  0001 C CNN
	1    5700 2650
	-1   0    0    -1  
$EndComp
$Comp
L electrode U10
U 1 1 5A81C4D5
P 5700 2750
F 0 "U10" H 5600 2750 60  0000 C CNN
F 1 "electrode" H 6500 2750 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 2750 60  0001 C CNN
F 3 "" H 5700 2750 60  0001 C CNN
	1    5700 2750
	-1   0    0    -1  
$EndComp
$Comp
L electrode U11
U 1 1 5A81C506
P 5700 2850
F 0 "U11" H 5600 2850 60  0000 C CNN
F 1 "electrode" H 6500 2850 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 2850 60  0001 C CNN
F 3 "" H 5700 2850 60  0001 C CNN
	1    5700 2850
	-1   0    0    -1  
$EndComp
$Comp
L electrode U12
U 1 1 5A81C9CA
P 5700 2950
F 0 "U12" H 5600 2950 60  0000 C CNN
F 1 "electrode" H 6500 2950 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 2950 60  0001 C CNN
F 3 "" H 5700 2950 60  0001 C CNN
	1    5700 2950
	-1   0    0    -1  
$EndComp
$Comp
L electrode U13
U 1 1 5A81C9D0
P 5700 3050
F 0 "U13" H 5600 3050 60  0000 C CNN
F 1 "electrode" H 6500 3050 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 3050 60  0001 C CNN
F 3 "" H 5700 3050 60  0001 C CNN
	1    5700 3050
	-1   0    0    -1  
$EndComp
$Comp
L electrode U14
U 1 1 5A81C9D6
P 5700 3150
F 0 "U14" H 5600 3150 60  0000 C CNN
F 1 "electrode" H 6500 3150 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 3150 60  0001 C CNN
F 3 "" H 5700 3150 60  0001 C CNN
	1    5700 3150
	-1   0    0    -1  
$EndComp
$Comp
L electrode U15
U 1 1 5A81C9DC
P 5700 3250
F 0 "U15" H 5600 3250 60  0000 C CNN
F 1 "electrode" H 6500 3250 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 3250 60  0001 C CNN
F 3 "" H 5700 3250 60  0001 C CNN
	1    5700 3250
	-1   0    0    -1  
$EndComp
$Comp
L electrode U16
U 1 1 5A81C9E2
P 5700 3350
F 0 "U16" H 5600 3350 60  0000 C CNN
F 1 "electrode" H 6500 3350 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 3350 60  0001 C CNN
F 3 "" H 5700 3350 60  0001 C CNN
	1    5700 3350
	-1   0    0    -1  
$EndComp
$Comp
L electrode U17
U 1 1 5A81C9E8
P 5700 3450
F 0 "U17" H 5600 3450 60  0000 C CNN
F 1 "electrode" H 6500 3450 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 3450 60  0001 C CNN
F 3 "" H 5700 3450 60  0001 C CNN
	1    5700 3450
	-1   0    0    -1  
$EndComp
$Comp
L electrode U18
U 1 1 5A81C9EE
P 5700 3550
F 0 "U18" H 5600 3550 60  0000 C CNN
F 1 "electrode" H 6500 3550 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 3550 60  0001 C CNN
F 3 "" H 5700 3550 60  0001 C CNN
	1    5700 3550
	-1   0    0    -1  
$EndComp
$Comp
L electrode U19
U 1 1 5A81C9F4
P 5700 3650
F 0 "U19" H 5600 3650 60  0000 C CNN
F 1 "electrode" H 6500 3650 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 3650 60  0001 C CNN
F 3 "" H 5700 3650 60  0001 C CNN
	1    5700 3650
	-1   0    0    -1  
$EndComp
$Comp
L electrode U21
U 1 1 5A81C9FA
P 5700 3750
F 0 "U21" H 5600 3750 60  0000 C CNN
F 1 "electrode" H 6500 3750 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 3750 60  0001 C CNN
F 3 "" H 5700 3750 60  0001 C CNN
	1    5700 3750
	-1   0    0    -1  
$EndComp
$Comp
L electrode U23
U 1 1 5A81CA00
P 5700 3850
F 0 "U23" H 5600 3850 60  0000 C CNN
F 1 "electrode" H 6500 3850 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 3850 60  0001 C CNN
F 3 "" H 5700 3850 60  0001 C CNN
	1    5700 3850
	-1   0    0    -1  
$EndComp
$Comp
L electrode U25
U 1 1 5A81CA06
P 5700 3950
F 0 "U25" H 5600 3950 60  0000 C CNN
F 1 "electrode" H 6500 3950 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 3950 60  0001 C CNN
F 3 "" H 5700 3950 60  0001 C CNN
	1    5700 3950
	-1   0    0    -1  
$EndComp
$Comp
L electrode U20
U 1 1 5A81CE0B
P 5700 3650
F 0 "U20" H 5600 3650 60  0000 C CNN
F 1 "electrode" H 6500 3650 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 3650 60  0001 C CNN
F 3 "" H 5700 3650 60  0001 C CNN
	1    5700 3650
	-1   0    0    -1  
$EndComp
$Comp
L electrode U22
U 1 1 5A81CE11
P 5700 3750
F 0 "U22" H 5600 3750 60  0000 C CNN
F 1 "electrode" H 6500 3750 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 3750 60  0001 C CNN
F 3 "" H 5700 3750 60  0001 C CNN
	1    5700 3750
	-1   0    0    -1  
$EndComp
$Comp
L electrode U24
U 1 1 5A81CE17
P 5700 3850
F 0 "U24" H 5600 3850 60  0000 C CNN
F 1 "electrode" H 6500 3850 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 3850 60  0001 C CNN
F 3 "" H 5700 3850 60  0001 C CNN
	1    5700 3850
	-1   0    0    -1  
$EndComp
$Comp
L electrode U26
U 1 1 5A81CE1D
P 5700 3950
F 0 "U26" H 5600 3950 60  0000 C CNN
F 1 "electrode" H 6500 3950 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 3950 60  0001 C CNN
F 3 "" H 5700 3950 60  0001 C CNN
	1    5700 3950
	-1   0    0    -1  
$EndComp
$Comp
L electrode U27
U 1 1 5A81CE23
P 5700 4050
F 0 "U27" H 5600 4050 60  0000 C CNN
F 1 "electrode" H 6500 4050 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 4050 60  0001 C CNN
F 3 "" H 5700 4050 60  0001 C CNN
	1    5700 4050
	-1   0    0    -1  
$EndComp
$Comp
L electrode U28
U 1 1 5A81CE29
P 5700 4150
F 0 "U28" H 5600 4150 60  0000 C CNN
F 1 "electrode" H 6500 4150 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 4150 60  0001 C CNN
F 3 "" H 5700 4150 60  0001 C CNN
	1    5700 4150
	-1   0    0    -1  
$EndComp
$Comp
L electrode U29
U 1 1 5A81CE2F
P 5700 4250
F 0 "U29" H 5600 4250 60  0000 C CNN
F 1 "electrode" H 6500 4250 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 4250 60  0001 C CNN
F 3 "" H 5700 4250 60  0001 C CNN
	1    5700 4250
	-1   0    0    -1  
$EndComp
$Comp
L electrode U30
U 1 1 5A81CE35
P 5700 4350
F 0 "U30" H 5600 4350 60  0000 C CNN
F 1 "electrode" H 6500 4350 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 4350 60  0001 C CNN
F 3 "" H 5700 4350 60  0001 C CNN
	1    5700 4350
	-1   0    0    -1  
$EndComp
$Comp
L electrode U31
U 1 1 5A81CE3B
P 5700 4450
F 0 "U31" H 5600 4450 60  0000 C CNN
F 1 "electrode" H 6500 4450 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 4450 60  0001 C CNN
F 3 "" H 5700 4450 60  0001 C CNN
	1    5700 4450
	-1   0    0    -1  
$EndComp
$Comp
L electrode U32
U 1 1 5A81CE41
P 5700 4550
F 0 "U32" H 5600 4550 60  0000 C CNN
F 1 "electrode" H 6500 4550 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 4550 60  0001 C CNN
F 3 "" H 5700 4550 60  0001 C CNN
	1    5700 4550
	-1   0    0    -1  
$EndComp
$Comp
L electrode U33
U 1 1 5A81CE47
P 5700 4650
F 0 "U33" H 5600 4650 60  0000 C CNN
F 1 "electrode" H 6500 4650 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 4650 60  0001 C CNN
F 3 "" H 5700 4650 60  0001 C CNN
	1    5700 4650
	-1   0    0    -1  
$EndComp
$Comp
L electrode U34
U 1 1 5A81CE4D
P 5700 4750
F 0 "U34" H 5600 4750 60  0000 C CNN
F 1 "electrode" H 6500 4750 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 4750 60  0001 C CNN
F 3 "" H 5700 4750 60  0001 C CNN
	1    5700 4750
	-1   0    0    -1  
$EndComp
$Comp
L electrode U35
U 1 1 5A81CE53
P 5700 4850
F 0 "U35" H 5600 4850 60  0000 C CNN
F 1 "electrode" H 6500 4850 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 4850 60  0001 C CNN
F 3 "" H 5700 4850 60  0001 C CNN
	1    5700 4850
	-1   0    0    -1  
$EndComp
$Comp
L electrode U36
U 1 1 5A81CE59
P 5700 4950
F 0 "U36" H 5600 4950 60  0000 C CNN
F 1 "electrode" H 6500 4950 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 4950 60  0001 C CNN
F 3 "" H 5700 4950 60  0001 C CNN
	1    5700 4950
	-1   0    0    -1  
$EndComp
$Comp
L electrode U37
U 1 1 5A81CE5F
P 5700 5050
F 0 "U37" H 5600 5050 60  0000 C CNN
F 1 "electrode" H 6500 5050 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 5050 60  0001 C CNN
F 3 "" H 5700 5050 60  0001 C CNN
	1    5700 5050
	-1   0    0    -1  
$EndComp
$Comp
L electrode U38
U 1 1 5A81CE65
P 5700 5150
F 0 "U38" H 5600 5150 60  0000 C CNN
F 1 "electrode" H 6500 5150 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 5150 60  0001 C CNN
F 3 "" H 5700 5150 60  0001 C CNN
	1    5700 5150
	-1   0    0    -1  
$EndComp
$Comp
L electrode U39
U 1 1 5A81CE6B
P 5700 5250
F 0 "U39" H 5600 5250 60  0000 C CNN
F 1 "electrode" H 6500 5250 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 5250 60  0001 C CNN
F 3 "" H 5700 5250 60  0001 C CNN
	1    5700 5250
	-1   0    0    -1  
$EndComp
$Comp
L electrode U40
U 1 1 5A81CE71
P 5700 5350
F 0 "U40" H 5600 5350 60  0000 C CNN
F 1 "electrode" H 6500 5350 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 5350 60  0001 C CNN
F 3 "" H 5700 5350 60  0001 C CNN
	1    5700 5350
	-1   0    0    -1  
$EndComp
$Comp
L electrode U41
U 1 1 5A81CE77
P 5700 5450
F 0 "U41" H 5600 5450 60  0000 C CNN
F 1 "electrode" H 6500 5450 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 5450 60  0001 C CNN
F 3 "" H 5700 5450 60  0001 C CNN
	1    5700 5450
	-1   0    0    -1  
$EndComp
$Comp
L electrode U42
U 1 1 5A81CE7D
P 5700 5550
F 0 "U42" H 5600 5550 60  0000 C CNN
F 1 "electrode" H 6500 5550 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 5550 60  0001 C CNN
F 3 "" H 5700 5550 60  0001 C CNN
	1    5700 5550
	-1   0    0    -1  
$EndComp
$Comp
L electrode U43
U 1 1 5A81CE83
P 5700 5650
F 0 "U43" H 5600 5650 60  0000 C CNN
F 1 "electrode" H 6500 5650 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 5650 60  0001 C CNN
F 3 "" H 5700 5650 60  0001 C CNN
	1    5700 5650
	-1   0    0    -1  
$EndComp
$Comp
L electrode U44
U 1 1 5A81CE89
P 5700 5750
F 0 "U44" H 5600 5750 60  0000 C CNN
F 1 "electrode" H 6500 5750 60  0000 C CNN
F 2 "measurement_electrodes:Measurement_Point_Round-SMD-Pad_Small" H 5700 5750 60  0001 C CNN
F 3 "" H 5700 5750 60  0001 C CNN
	1    5700 5750
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
