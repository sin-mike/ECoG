EESchema Schematic File Version 2
LIBS:_saved_probe2-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:a16pinout
LIBS:electrode
LIBS:_saved_probe2-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Entry Wire Line
	8150 -3450 8250 -3350
NoConn ~ 150  -4450
Entry Wire Line
	2300 -3100 2400 -3000
$Comp
L electrode U9
U 1 1 59B66E42
P 5750 1900
F 0 "U9" H 5750 2000 60  0000 C CNN
F 1 "electrode" H 5800 1800 60  0000 C CNN
F 2 "measurement_electrode_100mkm:measurement_electrode_100mkm" H 5750 1900 60  0001 C CNN
F 3 "" H 5750 1900 60  0001 C CNN
	1    5750 1900
	1    0    0    -1  
$EndComp
$Comp
L electrode U10
U 1 1 59B66EEA
P 5750 2150
F 0 "U10" H 5750 2250 60  0000 C CNN
F 1 "electrode" H 5800 2050 60  0000 C CNN
F 2 "measurement_electrode_100mkm:measurement_electrode_100mkm" H 5750 2150 60  0001 C CNN
F 3 "" H 5750 2150 60  0001 C CNN
	1    5750 2150
	1    0    0    -1  
$EndComp
$Comp
L electrode U11
U 1 1 59B66F1E
P 5750 2400
F 0 "U11" H 5750 2500 60  0000 C CNN
F 1 "electrode" H 5800 2300 60  0000 C CNN
F 2 "measurement_electrode_100mkm:measurement_electrode_100mkm" H 5750 2400 60  0001 C CNN
F 3 "" H 5750 2400 60  0001 C CNN
	1    5750 2400
	1    0    0    -1  
$EndComp
$Comp
L electrode U12
U 1 1 59B66F40
P 5750 2700
F 0 "U12" H 5750 2800 60  0000 C CNN
F 1 "electrode" H 5800 2600 60  0000 C CNN
F 2 "measurement_electrode_100mkm:measurement_electrode_100mkm" H 5750 2700 60  0001 C CNN
F 3 "" H 5750 2700 60  0001 C CNN
	1    5750 2700
	1    0    0    -1  
$EndComp
$Comp
L electrode U13
U 1 1 59B66F64
P 5750 3000
F 0 "U13" H 5750 3100 60  0000 C CNN
F 1 "electrode" H 5800 2900 60  0000 C CNN
F 2 "measurement_electrode_100mkm:measurement_electrode_100mkm" H 5750 3000 60  0001 C CNN
F 3 "" H 5750 3000 60  0001 C CNN
	1    5750 3000
	1    0    0    -1  
$EndComp
$Comp
L electrode U14
U 1 1 59B66F8D
P 5750 3250
F 0 "U14" H 5750 3350 60  0000 C CNN
F 1 "electrode" H 5800 3150 60  0000 C CNN
F 2 "measurement_electrode_100mkm:measurement_electrode_100mkm" H 5750 3250 60  0001 C CNN
F 3 "" H 5750 3250 60  0001 C CNN
	1    5750 3250
	1    0    0    -1  
$EndComp
$Comp
L electrode U15
U 1 1 59B66FAE
P 5750 3600
F 0 "U15" H 5750 3700 60  0000 C CNN
F 1 "electrode" H 5800 3500 60  0000 C CNN
F 2 "measurement_electrode_100mkm:measurement_electrode_100mkm" H 5750 3600 60  0001 C CNN
F 3 "" H 5750 3600 60  0001 C CNN
	1    5750 3600
	1    0    0    -1  
$EndComp
$Comp
L electrode U16
U 1 1 59B66FD5
P 5750 3950
F 0 "U16" H 5750 4050 60  0000 C CNN
F 1 "electrode" H 5800 3850 60  0000 C CNN
F 2 "measurement_electrode_100mkm:measurement_electrode_100mkm" H 5750 3950 60  0001 C CNN
F 3 "" H 5750 3950 60  0001 C CNN
	1    5750 3950
	1    0    0    -1  
$EndComp
$Comp
L electrode U1
U 1 1 59B67659
P 4250 1900
F 0 "U1" H 4250 2000 60  0000 C CNN
F 1 "electrode" H 4300 1800 60  0000 C CNN
F 2 "measurement_electrode_100mkm:measurement_electrode_100mkm" H 4250 1900 60  0001 C CNN
F 3 "" H 4250 1900 60  0001 C CNN
	1    4250 1900
	-1   0    0    -1  
$EndComp
$Comp
L electrode U2
U 1 1 59B67660
P 4250 2150
F 0 "U2" H 4250 2250 60  0000 C CNN
F 1 "electrode" H 4300 2050 60  0000 C CNN
F 2 "measurement_electrode_100mkm:measurement_electrode_100mkm" H 4250 2150 60  0001 C CNN
F 3 "" H 4250 2150 60  0001 C CNN
	1    4250 2150
	-1   0    0    -1  
$EndComp
$Comp
L electrode U3
U 1 1 59B67667
P 4250 2400
F 0 "U3" H 4250 2500 60  0000 C CNN
F 1 "electrode" H 4300 2300 60  0000 C CNN
F 2 "measurement_electrode_100mkm:measurement_electrode_100mkm" H 4250 2400 60  0001 C CNN
F 3 "" H 4250 2400 60  0001 C CNN
	1    4250 2400
	-1   0    0    -1  
$EndComp
$Comp
L electrode U4
U 1 1 59B6766E
P 4250 2700
F 0 "U4" H 4250 2800 60  0000 C CNN
F 1 "electrode" H 4300 2600 60  0000 C CNN
F 2 "measurement_electrode_100mkm:measurement_electrode_100mkm" H 4250 2700 60  0001 C CNN
F 3 "" H 4250 2700 60  0001 C CNN
	1    4250 2700
	-1   0    0    -1  
$EndComp
$Comp
L electrode U5
U 1 1 59B67675
P 4250 3000
F 0 "U5" H 4250 3100 60  0000 C CNN
F 1 "electrode" H 4300 2900 60  0000 C CNN
F 2 "measurement_electrode_100mkm:measurement_electrode_100mkm" H 4250 3000 60  0001 C CNN
F 3 "" H 4250 3000 60  0001 C CNN
	1    4250 3000
	-1   0    0    -1  
$EndComp
$Comp
L electrode U6
U 1 1 59B6767C
P 4250 3250
F 0 "U6" H 4250 3350 60  0000 C CNN
F 1 "electrode" H 4300 3150 60  0000 C CNN
F 2 "measurement_electrode_100mkm:measurement_electrode_100mkm" H 4250 3250 60  0001 C CNN
F 3 "" H 4250 3250 60  0001 C CNN
	1    4250 3250
	-1   0    0    -1  
$EndComp
$Comp
L electrode U7
U 1 1 59B67683
P 4250 3600
F 0 "U7" H 4250 3700 60  0000 C CNN
F 1 "electrode" H 4300 3500 60  0000 C CNN
F 2 "measurement_electrode_100mkm:measurement_electrode_100mkm" H 4250 3600 60  0001 C CNN
F 3 "" H 4250 3600 60  0001 C CNN
	1    4250 3600
	-1   0    0    -1  
$EndComp
$Comp
L electrode U8
U 1 1 59B6768A
P 4250 3950
F 0 "U8" H 4250 4050 60  0000 C CNN
F 1 "electrode" H 4300 3850 60  0000 C CNN
F 2 "measurement_electrode_100mkm:measurement_electrode_100mkm" H 4250 3950 60  0001 C CNN
F 3 "" H 4250 3950 60  0001 C CNN
	1    4250 3950
	-1   0    0    -1  
$EndComp
$Comp
L A16pinout A161
U 1 1 59B676A9
P 5100 2950
F 0 "A161" H 5050 3500 60  0000 C CNN
F 1 "A16pinout" H 5050 2600 60  0000 C CNN
F 2 "Modules:Pololu_Breakout-16_15.2x20.3mm" H 5100 2650 60  0001 C CNN
F 3 "" H 5100 2650 60  0001 C CNN
	1    5100 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 1900 5350 1900
Wire Wire Line
	5350 1900 5350 2500
Wire Wire Line
	5750 2150 5400 2150
Wire Wire Line
	5400 2150 5400 2600
Wire Wire Line
	5400 2600 5350 2600
Wire Wire Line
	5750 2400 5450 2400
Wire Wire Line
	5450 2400 5450 2700
Wire Wire Line
	5450 2700 5350 2700
Wire Wire Line
	5750 2700 5500 2700
Wire Wire Line
	5500 2700 5500 2800
Wire Wire Line
	5500 2800 5350 2800
Wire Wire Line
	5750 3000 5600 3000
Wire Wire Line
	5600 3000 5600 2900
Wire Wire Line
	5600 2900 5350 2900
Wire Wire Line
	5750 3250 5550 3250
Wire Wire Line
	5550 3250 5550 3000
Wire Wire Line
	5550 3000 5350 3000
Wire Wire Line
	5750 3600 5500 3600
Wire Wire Line
	5500 3600 5500 3100
Wire Wire Line
	5500 3100 5350 3100
Wire Wire Line
	5750 3950 5450 3950
Wire Wire Line
	5450 3950 5450 3200
Wire Wire Line
	5450 3200 5350 3200
Wire Wire Line
	4250 1900 4700 1900
Wire Wire Line
	4700 1900 4700 2500
Wire Wire Line
	4250 2150 4650 2150
Wire Wire Line
	4650 2150 4650 2600
Wire Wire Line
	4650 2600 4700 2600
Wire Wire Line
	4250 2400 4600 2400
Wire Wire Line
	4600 2400 4600 2700
Wire Wire Line
	4600 2700 4700 2700
Wire Wire Line
	4250 2700 4550 2700
Wire Wire Line
	4550 2700 4550 2800
Wire Wire Line
	4550 2800 4700 2800
Wire Wire Line
	4250 3000 4400 3000
Wire Wire Line
	4400 3000 4400 2900
Wire Wire Line
	4400 2900 4700 2900
Wire Wire Line
	4250 3250 4450 3250
Wire Wire Line
	4450 3250 4450 3000
Wire Wire Line
	4450 3000 4700 3000
Wire Wire Line
	4250 3600 4550 3600
Wire Wire Line
	4550 3600 4550 3100
Wire Wire Line
	4550 3100 4700 3100
Wire Wire Line
	4250 3950 4700 3950
Wire Wire Line
	4700 3950 4700 3200
$EndSCHEMATC
