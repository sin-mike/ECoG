EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:samtec_molc_110_01_s_q
LIBS:a16pinout
LIBS:electrode
LIBS:ground_enclosure
LIBS:64 channel-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ECOG_ACx_3600_2000_60ch ECOG1
U 1 1 5A422315
P 5450 2300
F 0 "ECOG1" H 5450 2500 60  0000 C CNN
F 1 "ECOG_ACx_3600_2000_60ch" H 5500 2650 60  0000 C CNN
F 2 "measurement_electrodes:ECOG_ACx_3600_2000_60" H 5450 2300 60  0001 C CNN
F 3 "" H 5450 2300 60  0001 C CNN
	1    5450 2300
	1    0    0    -1  
$EndComp
$Comp
L SAMTEC_MOLC_110_01_S_Q MOLC2
U 1 1 5A422626
P 2150 1750
F 0 "MOLC2" H 2300 1950 60  0000 C CNN
F 1 "SAMTEC_MOLC_110_01_S_Q" H 2350 2100 60  0000 C CNN
F 2 "SAMTEC:SAMTEC_MOLC_110_01_s_q" H 2200 2500 60  0001 C CNN
F 3 "" H 2200 2500 60  0001 C CNN
	1    2150 1750
	-1   0    0    -1  
$EndComp
$Comp
L SAMTEC_MOLC_110_01_S_Q MOLC1
U 1 1 5A422791
P 8800 1750
F 0 "MOLC1" H 8900 1950 60  0000 C CNN
F 1 "SAMTEC_MOLC_110_01_S_Q" H 8850 2100 60  0000 C CNN
F 2 "SAMTEC:SAMTEC_MOLC_110_01_s_q" H 8850 2500 60  0001 C CNN
F 3 "" H 8850 2500 60  0001 C CNN
	1    8800 1750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
